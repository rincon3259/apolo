from datetime import datetime

from crum import get_current_request
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator

from core.pos.models import Company


class IsSuperuserMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        return redirect('index')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['date_now'] = datetime.now()
        return context


class ValidatePermissionRequiredMixin(object):
    permission_required = ''
    url_redirect = None

    def get_perms(self):
        perms = []
        if isinstance(self.permission_required, str):
            perms.append(self.permission_required)
        else:
            perms = list(self.permission_required)
        return perms

    def get_url_redirect(self):
        if self.url_redirect is None:
            return reverse_lazy('dashboard')
        return self.url_redirect

    @method_decorator(login_required) #Decorador para validar el logueo del usuario.
    def get(self, request, *args, **kwargs):
        request = get_current_request()
        if request.user.is_superuser:
            return super().get(request, *args, **kwargs)
        if 'group' in request.session:
            group = request.session['group']
            perms = self.get_perms()
            for p in perms:
                if not group.permissions.filter(codename=p).exists():
                    messages.error(request, 'No tiene permiso para ingresar a este módulo')
                    return HttpResponseRedirect(self.get_url_redirect())
            return super().get(request, *args, **kwargs)
        messages.error(request, 'No tiene permiso para ingresar a este módulo')
        return HttpResponseRedirect(self.get_url_redirect())


class ExistsCompanyMixin(object): #valida si existe una compañía registrada o creada
    def dispatch(self, request, *args, **kwargs):
        if Company.objects.all().exists():#Si compañia tiene un dato significa que esta correcto
            return super().dispatch(request, *args, **kwargs)#retorna a la vista de ventas
        messages.error(request, 'No se puede facturar si no está registrada la compañía')#en caso que no haya datos en el módulo compañia mostrará este mensaje de error
        return redirect('dashboard')#en caso que no haya datos en el módulo compañia retornara al dashboard
