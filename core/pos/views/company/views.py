from django.http import JsonResponse #importar clase JsonResponse de solicitudes de peticiones.
from django.urls import reverse_lazy #importar librería de redireccionamiento.
from django.views.generic import UpdateView

from core.pos.forms import CompanyForm #importar el formulario que se necesita.
from core.pos.mixins import ValidatePermissionRequiredMixin #importar librería.
from core.pos.models import Company #importar el modelo que se necesita.


class CompanyUpdateView(ValidatePermissionRequiredMixin, UpdateView):
    model = Company #importamos el modelo.
    form_class = CompanyForm #importamos el formulario.
    template_name = 'company/create.html' #importamos la estructura de la vista.
    success_url = reverse_lazy('dashboard') #para que retorne a la vista principal.
    url_redirect = success_url
    permission_required = 'change_company' #Este es el permiso que tiene la vista para interactuar con el módulo "Base de datos, actualizar y ver los campos".

    def get_object(self, queryset=None): #Método que envía una instancia a la url se utiliza para validar el
        company = Company.objects.all() #atributo company que va a tener un listado de compañías.
        if company.exists(): #se valida si company tiene datos.
            return company[0] #Si tiene datos retorna el objeto compañia pero de uno solo porque el método get_object solo necesita una sola instancia
        return Company() #si no tiene datos, se envía la instancia como una compañía vacia.

    def post(self, request, *args, **kwargs): #El método post envía datos que el usuario no ve.
        data = {}
        try:
            action = request.POST['action']#trae la acción del boton guardar registro
            if action == 'edit':#valida que los campos del formulario esten llenos para guardar los datos
                instance = self.get_object()#variable para almacenar el objeto de la instancia del dato que estamos editando
                if instance.pk is not None: #Se valida si la clave primaria esta vacia, de no estarilo se realiza un editar porque esta llegando con dartos.
                    form = CompanyForm(request.POST, request.FILES, instance=instance) #recive los datos del formulario con el id que el objeto get_object trae.
                    data = form.save() #Guarda los datos del formulario.
                else:#si esta vacio crea los datos.
                    form = CompanyForm(request.POST, request.FILES)#recive los datos del formulario
                    data = form.save()#Guarda los datos del formulario.
            else:#En caso de estar el formulario vacio se muestran mensajes de error.
                data['error'] = 'No ha ingresado a ninguna opción'
        except Exception as e:
            data['error'] = str(e)
        return JsonResponse(data) #la clase JsonResponse valida el envío de la petición.

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Registro de mi compañía'
        context['entity'] = 'Compañía' #Entidad.
        context['list_url'] = self.success_url
        context['action'] = 'edit' #acción.
        return context
