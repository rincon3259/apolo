from django.urls import path
from core.api.views import *
from core.api.routers import router #Importamos el metodo Router

urlpatterns = [
    path('category/', CategoryAPIView.as_view(), name='api_category'),#Vista base genérica implementando la clase APIView
    path('category/list/', CategoryListAPIView.as_view(), name='api_category_list'), #vista para lista de categorías.
    path('category/create/', CategoryCreateAPIView.as_view(), name='api_category_create'), #Vista para crear datos en la instancia categorías.
    path('category/update/<int:pk>/', CategoryUpdateAPIView.as_view(), name='api_category_update'), #vista para actualizar categorías.
    path('category/delete/<int:pk>/', CategoryDestroyAPIView.as_view(), name='api_category_delete'), #vista para borrar categorías.
    path('product/list/', ProductListAPIView.as_view(), name='api_product_list'), #lista de productos.
]


urlpatterns += router.urls #LlAmamos el archivo router para implementar APIs mediante ViewSets y poder hacer uso de todas las URLs
