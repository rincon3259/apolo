from rest_framework import serializers #importamos los serializadores.

from core.pos.models import Category, Product, Client #Importa las entidades con las que vamos a trabajar

#Serializador de la instancia categorias
class CategorySerializers(serializers.ModelSerializer): #Serializador se parece a los formularios, especificamos el modelo y las columnas que queremos que retornen, esos datos se transforman se serializan y los presentamos cuando nuestra API ya este construida.
    class Meta: #Declaramos las propiedades
        model = Category #modelo a trabajar.
        fields = '__all__' #Columnas en este caso son todas las columnas.


class ClientSerializers(serializers.ModelSerializer):#Serializador se parece a los formularios, especificamos el modelo y las columnas que queremos que retornen, esos datos se transforman se serializan y los presentamos cuando nuestra API ya este construida.
    class Meta:
        model = Client
        fields = '__all__' #todas las filas.

    def to_representation(self, instance):
        return instance.toJSON() #Devuelve la instancia.

#Serializador de la instancia productos
class ProductSerializers(serializers.ModelSerializer):#El serializador permite definir al detalle cual es la respuesta que va a retornar mi API.
    class Meta: #Declaramos las propiedades.
        model = Product #modelo a trabajar.
        fields = '__all__' #Columnas en este caso son todas las columnas.

    def to_representation(self, instance): #Metodo to_representation de los serializadores permite saber como se va a retornar la información de la instancia de la entidad
        return instance.toJSON() #Retorna la instancia en metodo toJSON() con esto nos devuelve la ruta de las imagenes, Con esto no tenemos que sobre escribir el metodo get o el metodo post.
