from rest_framework.routers import DefaultRouter #importamos una variable.

from core.api.views import ClientViewSet #Importamos el el ViewSet creado

router = DefaultRouter() #Definimos la instancia de la variable
router.register(r'client', ClientViewSet, basename='client') #Registramos nuestra url mediante el ViewSets creado de clientes.
