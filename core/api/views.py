from rest_framework.authentication import TokenAuthentication #Llamamos el modelo para utilizar el token de autenticación perteneciente a Django Rest Framework.
from rest_framework.generics import ListAPIView, CreateAPIView, UpdateAPIView, DestroyAPIView #metodos de Django Rest Framework vistas genericas
from rest_framework.permissions import IsAuthenticated #Importa el permiso.
from rest_framework.response import Response
from rest_framework.views import APIView #Importamos la clase APIView que nos permite implementar una vista base para modificar los metodos .post() y .get() con mayor libertad
from rest_framework.viewsets import ModelViewSet

from core.api.serializers import * #De esta manera se llama a todos los serializadores del archivo core\api\serializers.py
from core.pos.models import Category, Product, Client #Importa las entidades con las que vamos a trabajar


class ClientViewSet(ModelViewSet): #Vista para implementar nuestra API de cliente mediante ModelViewSet.
    queryset = Client.objects.all()
    serializer_class = ClientSerializers

    def list(self, request, *args, **kwargs):
        return Response({'id': 4})


#Clase para vista genéricas de listado de categorías.
class CategoryListAPIView(ListAPIView): #ListAPIView es un método de Django Rest Framework para visualizar datos de las entidades
    queryset = Category.objects.all() #Enviamos los datos que necesitará nuestra API para presentar la lista de categorías en una url que será visualizada en el navegador.
    serializer_class = CategorySerializers #Serializar los datos, permite saber al detalle cómo va a devolver la respuesta de los objetos para retornarlos de la API. esto se hace en el archivo "core\api\serializers.py"
    authentication_classes = [TokenAuthentication] #Token de seguridad para darle seguridad a nuestra API
    permission_classes = [IsAuthenticated] #Asigna el permiso de la API IsAuthenticated Válida si el usuario existe y si también está logueado.
    def get(self, request, *args, **kwargs): #self hace referencia a la vista.
        return self.list(request, *args, **kwargs)

    def post(self, request, *args, **kwargs): #self hace referencia a la vista.
        print(self.queryset)
        serializer = self.serializer_class(self.queryset.all(), many=True)
        return Response(serializer.data)

#Clase para vista genericas de listado de productos.
class ProductListAPIView(ListAPIView): #ListAPIView es un metodo de Django Rest Framework para visualizar datos de las entidades
    queryset = Product.objects.all() #Enviamos los datos que necesitara nuestra API para presentar la lista de productos en una url que sera visualizada en el navegador.
    serializer_class = ProductSerializers #Serializar los datos, permite saber al detalle como va a devolver la respuesta de los objetos para retornarlos de la API. esto se hace en el archivo "core\api\serializers.py"


class CategoryCreateAPIView(CreateAPIView):#CreateAPIView es un metodo de Django Rest Framework para crear datos en las entidades
    serializer_class = CategorySerializers#Serializar los datos, permite saber al detalle como va a devolver la respuesta de los objetos para retornarlos de la API. esto se hace en el archivo "core\api\serializers.py"

    def post(self, request, *args, **kwargs): #Sobreescribimos el metodo POST para crear los datos en la entidad categoria
        print(self.request.data)
        return self.create(request, *args, **kwargs)


class CategoryUpdateAPIView(UpdateAPIView):#Vista Generica para modificar categorias.
    queryset = Category.objects.all() #Enviamos los datos que necesitara nuestra API para presentar la lista de categorias en una url que sera visualizada en el navegador.
    serializer_class = CategorySerializers #Serializar los datos, permite saber al detalle como va a devolver la respuesta de los objetos para retornarlos de la API. esto se hace en el archivo "core\api\serializers.py"

    def put(self, request, *args, **kwargs): #Estamos sobre escribiendo el metodo PUT
        print(self.request.data)
        return self.update(request, *args, **kwargs)


class CategoryDestroyAPIView(DestroyAPIView):#Vista generica para borrar categorias
    queryset = Category.objects.all() #Enviamos los datos que necesitara nuestra API para presentar la lista de categorias en una url que sera visualizada en el navegador.
    serializer_class = CategorySerializers #Serializar los datos, permite saber al detalle como va a devolver la respuesta de los objetos para retornarlos de la API. esto se hace en el archivo "core\api\serializers.py"

    def delete(self, request, *args, **kwargs): #Estamos sobre escribiendo el metodo DELITE
        instance = self.queryset.get(pk=self.kwargs['pk'])
        instance.delete()
        return Response({'msg': f"Se ha eliminado correctamente el pk {self.kwargs['pk']}"}) #Respuesta que retorna el metodo Delite modificado


class CategoryAPIView(APIView):#Vista general APIView para presentar las categorías
    def get(self, request, *args, **kwargs): #Sobre escribiendo el metodo .get()
        print(self.request.query_params) #sacamos datos de la entidad.
        return Response({'resp': False}) #Retorna los datos para visualizarlos a la vista.

    def post(self, request, *args, **kwargs): #Sobreescribiendo el método .post()
        queryset = Category.objects.all() #Enviamos datos a la entidad.
        # serializer = CategorySerializers(queryset, many=True)
        serializer = [i.toJSON() for i in queryset] #Serializar los datos, permite saber al detalle cómo va a devolver la respuesta de los objetos para retornarlos de la API. esto se hace en el archivo "core\api\serializers.py"
        return Response(serializer) #Retorna los datos de la entidad para visualizarlos.
